Projet-5
JARVIS est un réveil intelligent.\
Ce prototype est capable après avoir parametré correctement l'assistant virtuel, de répondre à la plupart de vos demande en terme de rappel et réveil.\
Pour les préférences JARVIS fait appel à des modules API chacun spécifique à la préférences activée.\
Exemple :\
-météo -> wetaher-api\
-bourse -> yahoo-finance\
-actualité -> GoogleNews