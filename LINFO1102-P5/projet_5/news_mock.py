# -*- coding: utf-8 -*-
"""
news_mock
micro module servant a donner des news aléatoire
by Allegaert Hadrien aka RainMaker
"""
from projet_5 import user_infos_reader as read
from random import randint
from datetime import datetime

class News:
    def __init__(self):
        """
        """
        self.terro = read.get_option("terrorisme", "projet_5/bdd/user/user.txt")
        self.date = str(datetime.now())[:10]
        self.eco = read.get_option("economie", "projet_5/bdd/user/user.txt")
        self.gaming = read.get_option("gaming", "projet_5/bdd/user/user.txt")

    def __str__(self):
        """
        :return:
        """
        actu_terro = ["Washington augmente ses budgets de lutte contre le terrorisme: ",
                      "Terrorisme jihadiste : comment une politique européenne se met en place pour y faire face : Le rétablissement des contrôles aux frontières françaises est censé prendre fin mardi 30 avril. Cette mesure exceptionnelle, prise au lendemain des attentats du 13-Novembre à Paris et Saint-Denis, est renouvelée régulièrement par le gouvernement français qui dit faire face à une menace terroriste très prégnante. En février, le ministre de l'Intérieur Christophe Castaner affirmait que le gouvernement envisageait de demander une nouvelle prolongation de six mois, en vue du G7, fin août à Biarritz. Le maintien de ces contrôles aux limites géographiques d'un Etat membre de l'espace Schengen, où la libre circulation est en vigueur, est symbolique de la prise en compte croissante du risque terroriste au sein de l'Union européenne. ",
                      "L’Europe autorise l’expulsion d’un Algérien condamné pour terrorisme. La Cour européenne des droits de l’homme (CEDH) a validé, lundi 29 avril, l’expulsion d’un Algérien condamné pour terrorisme en France. Une première. Les juges de Strasbourg s’y étaient toujours refusés en raison du risque de torture.",
                      "Terrorisme : attentat déjoué contre les forces de l'ordre. Le parquet de Paris a annoncé avoir désamorcé un projet d'attaque imminente envers les forces de l'ordre en arrêtant quatre personnes vendredi.",
                      "Terrorisme : un compagnon de route d’Abaaoud devant le tribunal. À à peine 26 ans, Mehdi Aida, qui comparait détenu devant le tribunal correctionnel, a connu du beau monde en matière de terrorisme. Depuis 2013, il fréquentait les milieux islamistes. En août 2013, il a été contrôlé avec Jean-Louis Denis alors qu’il effectuait des collectes pour la cause islamiste."]

        actu_eco = ["L'économie de la zone euro démarre 2019 sur de bonnes surprises : L'économie de la zone euro a commencé 2019 sur une note positive, avec une accélération de la croissance au premier trimestre et une baisse du chômage en mars, selon des chiffres publiés mardi. Dans un contexte de tensions commerciales, alimentées par les menaces protectionnistes américaines et les incertitudes persistantes sur le Brexit, les chiffres de l'Office européen des statistiques, Eurostat, sont bien meilleurs qu'escomptés.",
                    "L'économie belge n'a progressé que de 0,2% en début d'année: Cela fait plusieurs mois en effet que se succèdent les révisions à la baisse des prévisions de croissance, pour la zone euro et donc également pour la Belgique. 'Nous avons réduit de 0,5% notre prévision de croissance pour la zone euro, soit de 1,9% à 1,4%. La Belgique peut difficilement échapper à cette tendance générale', explique Koen De Leus, économiste en chef chez BNP Paribas Fortis.",
                    "Les États-Unis veulent achever d’asphyxier l’économie iranienne : L’administration Trump ne s’en est jamais cachée. Elle veut aller jusqu’au bout de sa logique restrictive à l’égard de l’Iran. En sifflant la fin des dérogations - à compter de ce 1er mai - qui permettaient à huit pays de poursuivre leurs importations de brut iranien malgré leur régime de sanctions, les États-Unis veulent asphyxier jusqu’au bout l’économie iranienne en éliminant sa principale source de revenus.",
                    "Les inégalités ont nourri le populisme, croit le Prix Nobel d'économie : Ex-économiste en chef de la Banque mondiale et ancien conseiller de Bill Clinton, Joseph Stiglitz recevait hier à Montréal le prix Anthony-Atkinson pour l'égalité. Il a profité de l'occasion pour dresser un portrait des écarts de richesse abyssaux qui séparent le 1 % des Américains les plus riches de la vaste majorité de leurs compatriotes. Et pour faire la promotion de son plus récent livre, People, Power, and Profits (Peuple, pouvoir et profits) dans lequel il se fait le chantre d'un « capitalisme progressiste », redistribuant les fruits de la croissance économique pour réduire les inégalités économiques",
                    "Jean Hindriks, professeur d’économie à l’UCL: «Au fédéral, on avance d’un pas, on recule d’un pas» Professeur d’économie à l’UCL, Jean Hindriks est l’invité du Grand Oral RTBF/Le Soir ce samedi 27 avril sur La Première et ce dimanche 28 avril sur La Trois. Co-auteur du livre « Un projet pour la Belgique » (Institut Itinera), il dresse le bilan des différents exécutifs. "]

        actu_gaming = ["",
                       "",
                       "",
                       "",
                       ""]

        final_str = ""
        if self.eco == "True":
            final_str += actu_eco[randint(0, 4)]
        if self.terro == "True":
            final_str += actu_terro[randint(0, 4)]
        if self.gaming == "True":
            final_str += actu_gaming[randint(0, 4)]
        return final_str