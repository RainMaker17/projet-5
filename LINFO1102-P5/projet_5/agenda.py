"""
Ce fichier sert a creer et utiliser un systeme d'agenda et de rappel
Not finish and not use in  P5 for the moment
"""
# import calendar
import datetime
import alarm


class Day:
    """
    un jour avec sa date et sa liste d'evenement.
    """
    def __init__(self, day_date, day_day, day_list):
        """

        """
        self.date = day_date
        self.day = day_day
        self.rappel = day_list

    def __str__(self):
        """
        pre:
        post: retourne une string du nombre de rendez vous du jour
        """
        return "Aujourd'hui, le {} vous avez {} rendez_vous".format(self.date, len(self.rappel))

    def meeting_list(self):
        """
        :return: liste des evenements d ela journee
        """
        return self.rappel

    def first_meet(self):
        """
        pre:
        post: retourne le premier evenement de la liste du jour
        """
        if not self.rappel:
            return "Vous n'avez pas de rappel aujourd'hui"
        temp_rappel = self.rappel[0]
        temp_rappel = temp_rappel.split(',')
        temp_hour = temp_rappel[1].split(':')[0][2:]
        temp_min = temp_rappel[1].split(':')[1][:-1]
        temp_lieu = temp_rappel[3][2:-3]
        f_str = "Aujourd'hui votre premier rendez vous est a {} heure {} a {}"
        return f_str.format(temp_hour, temp_min, temp_lieu)

    def reminder(self):
        """
        post: retourne une chaine de caracteres de tous les evenements de la journee
        """
        final_string = ""
        for i in enumerate(self.rappel):
            final_string += self.rappel[i[0]]
        return final_string


def active_reminder(filename):
    """
    active le rappel grace au module alarm.wake_up
    """
    return alarm.wake_up(filename)


def add_reminder(filename, content):
    """
    pre: filename = string path
        content = nouvel evenement
    post: appel la fonction d'ajout du module alarme
        et ajoute le nouvel evenement
    """
    return alarm.add_alarm(filename, content)


FILE_LIST = alarm.file_reader('bdd/date_file/{}.txt'.format(str(datetime.datetime.now())[:10]))
TESTED = Day(str(datetime.datetime.now())[:10], "DAYNAME", FILE_LIST)

print(TESTED)
print(TESTED.reminder())


if FILE_LIST:
    active_reminder("bdd/date_file/{}.txt".format(str(datetime.datetime.now())[:10]))
