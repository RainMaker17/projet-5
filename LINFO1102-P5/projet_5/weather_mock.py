# -*- coding: utf-8 -*-
"""
Mock de weather-api
Allegaert Hadrien aka RainMaker
"""
from datetime import datetime
from random import randint
from projet_5 import user_infos_reader as read


class Weather:
    """
    """
    def __init__(self, where = "position inconnue", temperatures=True, min_max_temp=True, rainchances=True):
        """

        """
        def month_stringer(month):
            """
            pre: month: str convertible en int
            post: une string du nom du mois ne cours
            """
            if int(month) == 1:
                return "janvier"
            elif int(month) == 2:
                return "février"
            elif int(month) == 3:
                return "mars"
            elif int(month) == 4:
                return "avril"
            elif int(month) == 5:
                return "mai"
            elif int(month) == 6:
                return "juin"
            elif int(month) == 7:
                return "juillet"
            elif int(month) == 8:
                return "aout"
            elif int(month) == 9:
                return "septembre"
            elif int(month) == 10:
                return "octobre"
            elif int(month) == 11:
                return "novembre"
            elif int(month) == 12:
                return "décembre"

        def abstract_creater():
            """
            :return: une string d'explication du temps (ex: ensoleillé)
            """
            abstract_list = ["ensoleillé", "nuageux", "enneigé", "tropical", "aux éclaircies", "brumeux"]
            return abstract_list[randint(0, 5)]

        temp = str(datetime.now().today())[:10]
        self.city = where
        self.date = int(temp[8:10])
        self.abstract = abstract_creater()
        self.month = temp[5:7]
        self.month_str = month_stringer(self.month)
        self.temperature = int()
        self.min_max = int()
        self.rainchances = int()

        if temperatures:
            self.temperature = randint(-5, 25)
        if min_max_temp and temperatures:
            self.min_max = randint(self.temperature-7, self.temperature)
        if rainchances:
            self.rainchances = randint(0, 100)

    def __str__(self):
        """
        """
        txt = "Aujourd'hui, le {} {} a {}, le temps est {}. "
        txt = txt.format(self.date, self.month_str, self.city, self.abstract)
        print(self.temperature)
        if read.get_option('temperature', "projet_5/bdd/user/user.txt") == "True":
            txt += "La température est de {} degrés. ".format(self.temperature)
        if read.get_option('min_max_temp', "projet_5/bdd/user/user.txt") == "True":
            txt += "Elle évoluera entre {} et {} degrés. ".format(self.min_max, self.min_max+7)
        if read.get_option('rain', "projet_5/bdd/user/user.txt") == "True":
            txt += "Les chances d'averces sont de {}%. ".format(self.rainchances)
        return txt
