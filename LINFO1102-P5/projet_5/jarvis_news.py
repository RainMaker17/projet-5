# -*- coding: utf-8 -*-
"""
jarvis_news
create string with news_mock
first version for GoogleNews api by Dourov Maxime
Erased by the version adapted to the news_mock
by Allegaert Hadrien aka RainMaker
"""
from projet_5.news_mock import News
from projet_5 import user_infos_reader as reader


def get_news():
    return str(News())