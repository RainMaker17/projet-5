# -*- coding: utf-8 -*-
"""
horoscope_mock
Générate pré defined Horoscope
Four list of horoscope one is randomly choose
and one of the sentence it content is randomly choose to be returned
by Allegaert Hadrien aka RainMaker
"""
from random import randint


class Horoscope:
    def __init__(self, user_sign):
        """
        :param user_sign: string like "sagittarius"
        """
        self.sign = user_sign

    def __str__(self):
        """
        :return:
        """
        list_one = ["Aujourd'hui, vous découvrirez pourquoi la vie est un toucan.",
                    "Décidemment la chance n'est aps de votre coté. Sortez couvert et ne vous apporchez pas de votre Tante.",
                    "Vous vous rendez compte que la vie n'est pas toujours rose, et vous allez enchainer les malheurs mais n'oubliez pas il vaut mieux en rire que d'en pleurer.",
                    "Célibataires, vous vivrez cette journée de manière intense et ne prendrez rien àla légère. Pas meme ce délicieux flirt qui vous est promis et qui vous entrainera probablement jusqu'au bout de la nuit.",
                    "Dernier jour de Lune descendante! Vous avez avantage à retarder vos projets de 24h. En effet, dés demain vous pourrez bouger, innover, et saisir toutes les occasions de vous faire remarquer.",
                    "Arreter de stresser, foncé voir votre dulcinée histoire de vous réconfortez un peu de ces journées de malheur.",
                    "Vous avez une révélations, les relations avec votre partenaires semblent plus légères. mais dés qu'il (elle) vous ennuis vous savez le (la) remettre à sa place.",
                    "Votre créativités sera enfin mise en avant et révélé au grand jour. Attention à ne pas prendre la grosse tete.",
                    "Célibataires il y a du changements, en couple aussi.",
                    "Vous regrettez toujorus certaines décision de la vie, comme ce premier rencard avec l'amour de votre vie, où vous avez disserté pendant deux heures sur le football. Dommage qu'elle s'en battait les coui* *bip*",
                    "L'heure est au dialogue, votre ancien grand amour reviendra vers vous. Attention à ce qu'il ait suffisament changé pour ne pas retombé dans les travers d'avant.",
                    "Mauvaise nouvelles : vous n'etes pas l'enfant caché de Kurt Cobain."]

        list_two = ["1B",
                    "2B",
                    "3B",
                    "4B",
                    "5B",
                    "6B",
                    "7B",
                    "8B",
                    "9B",
                    "10B",
                    "11B",
                    "12B"]

        list_three = ["1",
                      "2",
                      "3",
                      "4",
                      "5",
                      "6",
                      "7",
                      "8",
                      "9",
                      "10",
                      "11",
                      "12"]

        sign_list = ["aquarius", "pisces", "aries", "taurus", "gemini", "cancer",
                     "leo", "virgo", "libra", "scorpio", "sagittarius", "capricorn"]
        temp = 0
        for i in enumerate(sign_list):
            if sign_list[i[0]] == self.sign:
                temp = i[0] + 1
                break
        chooser = randint(0, 11)
        if temp > 0:
            if temp%3 == 0:
                return str(list_one[chooser])
            elif temp%2 == 0:
                return str(list_two[chooser])
            else:
                return str(list_three[chooser])
        else:
            return "An error occured"

