from projet_5.horoscope_mock import Horoscope as Horo
from projet_5 import user_infos_reader as read
"""
Horoscope api talker
by Dourov Maxime
Revised for use with mock by Allgeart Hadrien
"""


def get_sign(month, day):
    """
    :param month:
    :param day:
    :return:
    """
    signs = [
        ((1, 20), (2, 16), "aquarius"),
        ((2, 19), (3, 20), "pisces"),
        ((3, 21), (4, 19), "aries"),
        ((4, 20), (5, 20), "taurus"),
        ((5, 21), (6, 20), "gemini"),
        ((6, 21), (7, 22), "cancer"),
        ((7, 23), (8, 22), "leo"),
        ((8, 23), (9, 22), "virgo"),
        ((9, 23), (10, 22), "libra"),
        ((10, 23), (11, 21), "scorpio"),
        ((11, 22), (12, 21), "sagittarius"),
        ((12, 22), (1, 19), "capricorn")]

    for i in signs:
        if (i[0][0] == month and i[0][1] <= day) or (i[1][0] == month and i[1][1] >= day):
            return i[2]


def get_horoscope():
    """

    :return:
    """
    date = read.get_option('birth', "projet_5/bdd/user/user.txt")
    date_month = date[3:5]
    date_day = date [:2]
    sign = get_sign(int(date_month), int(date_day))
    final_str = "Horoscope du signe {}".format(sign)
    final_str += str(Horo(sign))
    return final_str
