from projet_5.weather_mock import Weather
from projet_5 import user_infos_reader as read
"""
Basis function to lookup some information on Weather mock
by Dourov Maxime
"""

def get_lookup():
	filename = "projet_5/bdd/user/user.txt"
	loc = read.get_option("location", filename)
	temp = read.get_option("temperature", filename)
	min_max = read.get_option("min_max_temp", filename)
	rain = read.get_option("rain", filename)
	return str(Weather(loc, temp, min_max, rain))

