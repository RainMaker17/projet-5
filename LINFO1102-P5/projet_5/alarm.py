# -*- coding: utf-8 -*-
"""
Module de réveil et diverses fonctions utiles : ajouter supprimer modifier reveil etc
Allegaert Hadrien aka RainMaker
"""
import time
import datetime


def tupler(date, hour, minute, title="NoTitle", lieu=None):
    """
    :param date: typo: 2000-01-15
    :param hour: int
    :param minute: int
    :param title: str
    :param lieu: str
    :return: simplified and typographied handle string
    """
    if int(minute) < 10 and int(hour) < 10:
        minute = '0{}'.format(int(minute))
        hour = '0{}'.format(int(hour))
    elif int(minute) < 10:
        minute = '0{}'.format(int(minute))
    elif int(hour) < 10:
        hour = '0{}'.format(int(hour))
    if lieu is not None:
        return "('{}', '{}:{}', '{}', '{}')".format(date, hour, minute, str(title), str(lieu))
    return "('{}', '{}:{}', '{}')".format(date, hour, minute, str(title))


def file_reader(filename):
    """
    Ouvre le fichier texte mentionner
    pre: filename path-file
    post: liste des lignes contenu dans le fichier
    """
    try:
        with open(filename, 'r') as open_file:
            file_list = open_file.readlines()
            open_file.close()
        return file_list

    except IOError:
        with open(filename, 'w') as open_file:
            open_file.close()
        return file_reader(filename)


def file_writer(filename, content):
    """
    Ecrit le contenu de content dans le fichier mentionner
    pre: list des lignes a ecrire ( non vide)
    post:
    """
    with open(filename, 'w') as open_file:
        for i in enumerate(content):
            open_file.write(str(content[i[0]]))
        open_file.close()


def add_alarm(filename, new_alarm):
    """
    Ajoute une alarme dans le fichier source
    pre: new_alarm ( syntaxe : "('year-month-day', 'hour:min', 'SONGPATH', 'Title')")
    post: retourne True si l'alarme a ete ajoutee et false si l'alarme existait deja
    """
    file_list = file_reader(filename)
    for i in enumerate(file_list):
        if str(new_alarm) == file_list[i[0]].rstrip():
            return False
    file_list.append(str(new_alarm)+'\n')
    file_list = sorted(file_list)
    file_writer(filename, file_list)
    return True


def del_alarm(filename, alarm):
    """
    Supprime une alarm du fichier source
    pre: la ligne a supprimer (syntaxe : "('year-month-day', 'hour:min', 'SONGPATH', 'Title')")
    post: retourne True si la ligne a ete supprimer, false si elle n'existe pas.
    """
    file_list = file_reader(filename)
    for i in enumerate(file_list):
        if str(alarm) == str(file_list[i[0]].strip()):
            del file_list[i[0]]
            file_writer(filename, file_list)
            return True
    return False


def modify_alarm(filename, alarm, new_alarm):
    """
    modifie une alarme existante
    pre: alarm = alarm a modifier new_alarm = alarme apres modification
        pour les deux la syntaxe: "('year-month-day', 'hour:min', 'SONGPATH', 'Title')"
    post: return True si l'alarme a ete modfier, false si l'alarme n'existe pas dans le fichier
        supprime l'ancienne alarme et renvoi une str si la nouvelle alarme existe deja
    """
    temp = del_alarm(filename, alarm)
    if temp:
        temp_two = add_alarm(filename, new_alarm)
        if temp_two:
            return True
        return str(temp_two) + ": was deleted instead"
    return False


def wake_up(filename):
    """
    prend la premiere ligne du fichier source
    la compare avec la date et l'heure actuelle
    calcule le temps a attendre avant de sonner
    effectue une unique recursion a la bonne heure
    pre: filename = path_file
    post: retourne True apres avoir supprimer
        l'alarme a la bonne date et heure
    """
    date = str(datetime.datetime.now())[:10]
    date_time = str(datetime.datetime.now())[11:16]
    temp = file_reader(filename)
    if not temp:
        return False
    temp_date = temp[0][2:12]
    temp_time = temp[0][16:21]
    if date == temp_date:
        if date_time == temp_time:
            del_alarm(filename, temp[0])
            return True
        if int(date_time[:2]) <= int(temp_time[:2]):
            sleeper = int(temp_time[:2]) - int(date_time[:2])
            if sleeper in (1, 0):
                if int(date_time[3:]) > int(temp_time[3:]):
                    min_sleeper = (60 - int(date_time[3:]) + int(temp_time[3:]))
                else:
                    min_sleeper = (int(temp_time[3:]) - int(date_time[3:]))
                time.sleep(min_sleeper*60)
                return wake_up(filename)
            time.sleep(sleeper*60*60)
            return wake_up(filename)
    # 24-hour(now)+hour(alarm)-1 remis en sec
    hour_sleeper = (24 - int(date_time[:2]) + int(temp_time[:2]) - 1) * 60 * 60
    # 60-min(now)+min(alarm) remis en s
    min_sleeper = (60 - int(date_time[3:]) + int(temp_time[3:])) * 60
    time.sleep(hour_sleeper + min_sleeper)
    return wake_up(filename)

def active_wake_up(filename="projet_5/bdd/user/alarm.txt"):
    wake_up(filename)
    return "le réveil est actif"
