# -*- coding: utf-8 -*-
"""
user info reader
by Dourov Maxime
"""

def read_file(filename):
    with open(filename, 'r') as open_file:
        file_list = open_file.readlines()
        open_file.close()
    return file_list


def write_file(filename, text):
    with open(filename, 'w') as open_file:
        open_file.write(str(text))
        open_file.close()


def get_option(option, filename="bdd/user/user.txt"):
    """
    retourne la string correspondant a l'option demandee
    retourne none si l'option n'aparait pas
    """
    options = {}
    try:
        for i in read_file(filename):
            opt = i.split("=")
            options[opt[0].strip()] = opt[1].strip()
        if options[option]:
                return options[option]
    except IOError:
        return "An error occured"

# def write_option(option,value,filename="user.txt"):
# pass
