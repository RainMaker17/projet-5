#!/user/bin/python
# coding: utf8
"""
Flask_ask_test provide by UClouvain EPL
Revisited and adapted to use with Jarvis Alarm
by Allegaert Hadrien
"""
from flask import Flask
from flask_ask import Ask, statement, question, audio

from projet_5 import alarm
from projet_5 import jarvis_horoscope as horo
from projet_5 import jarvis_weather as weath
from projet_5 import jarvis_news as news
from projet_5 import user_infos_reader as read
from threading import Thread
app = Flask(__name__)
ask = Ask(app, '/')



@ask.launch
def start():
    return question('Bonjour. Que puis-je faire pour vous ?')


@ask.intent('HelloIntent')
def bonjour(nom):
    if nom is None:
        return statement("Je ne comprends pas ce que vous voulez, désolé")
    return statement("Bonjour {}, je suis une intelligence artificielle programmée pour vous aider. Bonne journée.".format(nom))

@ask.intent('somme')
def somme_final(number_a, number_b):
    if number_a != None and number_b != None:
        return statement("La somme demandée est : {}".format(int(number_a) + int(number_b)))
    else:
        return statement("Je ne peux pas additionner cela!")

@ask.intent('multiply')
def multiply_result(number_a, number_b):
    if number_a != None and number_b != None:
        return statement("{} fois {} égale {}".format(number_a, number_b, int(number_a) * int(number_b)))
    else:
        return statement("Je ne peux pas multiplier cela!")

@ask.intent('AddAlarmIntent')
def add_alarm(date, heure):
    filename = "projet_5/bdd/user/alarm.txt"
    min = heure[3:]
    heure = heure[:2]
    temp = alarm.add_alarm(filename, alarm.tupler(date, heure, min))
    if temp is True:
        return statement("L'alarme a été correctement ajoutée")
    return statement("L'alarme n'a pas été ajoutée")

@ask.intent('DelAlarmIntent')
def del_alarm(del_date, del_heure):
    filename = "projet_5/bdd/user/alarm.txt"
    del_min = del_heure[3:]
    del_heure = del_heure[:2]
    temp_string = alarm.tupler(del_date, del_heure, del_min)
    temp = alarm.del_alarm(filename, temp_string)
    if temp is True:
        return statement("L'alarme à été supprimée")
    return statement("Désolé. L'alarme n'a pas été supprimée")

@ask.intent('ModifyAlarmIntent')
def modify_alarm(old_date, old_hour, new_date, new_hour):
    filename = "projet_5/bdd/user/alarm.txt"
    old_min = old_hour[3:]
    old_hour = old_hour[:2]
    new_min = new_hour[3:]
    new_hour = new_hour[:2]
    if alarm.modify_alarm(filename, alarm.tupler(old_date, old_hour, old_min),
                          alarm.tupler(new_date, new_hour, new_min)):
        return statement("L'alarme a été modifiée")
    if  not alarm.modify_alarm(filename, alarm.tupler(old_date, old_hour, old_min),
                          alarm.tupler(new_date, new_hour, new_min)):
        return statement("L'alarme a modifié n'a pas été trouvée")
    return statement("L'alarme a été supprimé car uen alarme identique à la nouvelle existait déjà")

@ask.intent('LaunchPrefIntent')
def launch_pref():
    filename = "projet_5/bdd/user/user.txt"
    temp_horo = ""
    temp_weather = ""
    temp_news = ""
    if read.get_option('horo', filename) == "True":
        temp_horo += "Horoscope : "
        temp_horo += horo.get_horoscope()
    if read.get_option('weather', filename) == "True":
        temp_weather += "Météo : "
        temp_weather += weath.get_lookup()
    if read.get_option('news', filename) == "True":
        temp_news += "Actualités : "
        temp_news += news.get_news()
    return statement("{}\n  {}\n  {}".format(temp_horo, temp_weather, temp_news))



if __name__ == '__main__':
    app.run()

